//
// Created by the DataSnap proxy generator.
// 11/06/2013 16:10:16
//

unit uProxy;

interface

uses Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, uCliente, uCidade, Data.DBXJSONReflect;

type
  TServerMethods1Client = class(TDSAdminClient)
  private
    FEchoStringCommand: TDBXCommand;
    FReverseStringCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
  end;

  TClienteServiceClient = class(TDSAdminClient)
  private
    FsaveCommand: TDBXCommand;
    FupdateCommand: TDBXCommand;
    FdeleteCommand: TDBXCommand;
    FfindbyidCommand: TDBXCommand;
    FAfterConstructionCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function save(Objeto: TCliente): TCliente;
    function update(Objeto: TCliente): TCliente;
    function delete(Objecto: TCliente): Boolean;
    function findbyid(ID: Integer): TCliente;
    procedure AfterConstruction;
  end;

  TCidadeServiceClient = class(TDSAdminClient)
  private
    FsaveCommand: TDBXCommand;
    FupdateCommand: TDBXCommand;
    FdeleteCommand: TDBXCommand;
    FfindbyidCommand: TDBXCommand;
    FAfterConstructionCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function save(Objeto: TCidade): TCidade;
    function update(Objeto: TCidade): TCidade;
    function delete(Objecto: TCidade): Boolean;
    function findbyid(ID: Integer): TCidade;
    procedure AfterConstruction;
  end;

implementation

function TServerMethods1Client.EchoString(Value: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FDBXConnection.CreateCommand;
    FEchoStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEchoStringCommand.Text := 'TServerMethods1.EchoString';
    FEchoStringCommand.Prepare;
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.ExecuteUpdate;
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethods1Client.ReverseString(Value: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FDBXConnection.CreateCommand;
    FReverseStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FReverseStringCommand.Text := 'TServerMethods1.ReverseString';
    FReverseStringCommand.Prepare;
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.ExecuteUpdate;
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;


constructor TServerMethods1Client.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TServerMethods1Client.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TServerMethods1Client.Destroy;
begin
  FreeAndNil(FEchoStringCommand);
  FreeAndNil(FReverseStringCommand);
  inherited;
end;

function TClienteServiceClient.save(Objeto: TCliente): TCliente;
begin
  if FsaveCommand = nil then
  begin
    FsaveCommand := FDBXConnection.CreateCommand;
    FsaveCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FsaveCommand.Text := 'TClienteService.save';
    FsaveCommand.Prepare;
  end;
  if not Assigned(Objeto) then
    FsaveCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FsaveCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FsaveCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Objeto), True);
      if FInstanceOwner then
        Objeto.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FsaveCommand.ExecuteUpdate;
  if not FsaveCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FsaveCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCliente(FUnMarshal.UnMarshal(FsaveCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FsaveCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TClienteServiceClient.update(Objeto: TCliente): TCliente;
begin
  if FupdateCommand = nil then
  begin
    FupdateCommand := FDBXConnection.CreateCommand;
    FupdateCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FupdateCommand.Text := 'TClienteService.update';
    FupdateCommand.Prepare;
  end;
  if not Assigned(Objeto) then
    FupdateCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FupdateCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FupdateCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Objeto), True);
      if FInstanceOwner then
        Objeto.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FupdateCommand.ExecuteUpdate;
  if not FupdateCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FupdateCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCliente(FUnMarshal.UnMarshal(FupdateCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FupdateCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TClienteServiceClient.delete(Objecto: TCliente): Boolean;
begin
  if FdeleteCommand = nil then
  begin
    FdeleteCommand := FDBXConnection.CreateCommand;
    FdeleteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FdeleteCommand.Text := 'TClienteService.delete';
    FdeleteCommand.Prepare;
  end;
  if not Assigned(Objecto) then
    FdeleteCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FdeleteCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FdeleteCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Objecto), True);
      if FInstanceOwner then
        Objecto.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FdeleteCommand.ExecuteUpdate;
  Result := FdeleteCommand.Parameters[1].Value.GetBoolean;
end;

function TClienteServiceClient.findbyid(ID: Integer): TCliente;
begin
  if FfindbyidCommand = nil then
  begin
    FfindbyidCommand := FDBXConnection.CreateCommand;
    FfindbyidCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FfindbyidCommand.Text := 'TClienteService.findbyid';
    FfindbyidCommand.Prepare;
  end;
  FfindbyidCommand.Parameters[0].Value.SetInt32(ID);
  FfindbyidCommand.ExecuteUpdate;
  if not FfindbyidCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FfindbyidCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCliente(FUnMarshal.UnMarshal(FfindbyidCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FfindbyidCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TClienteServiceClient.AfterConstruction;
begin
  if FAfterConstructionCommand = nil then
  begin
    FAfterConstructionCommand := FDBXConnection.CreateCommand;
    FAfterConstructionCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAfterConstructionCommand.Text := 'TClienteService.AfterConstruction';
    FAfterConstructionCommand.Prepare;
  end;
  FAfterConstructionCommand.ExecuteUpdate;
end;


constructor TClienteServiceClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TClienteServiceClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TClienteServiceClient.Destroy;
begin
  FreeAndNil(FsaveCommand);
  FreeAndNil(FupdateCommand);
  FreeAndNil(FdeleteCommand);
  FreeAndNil(FfindbyidCommand);
  FreeAndNil(FAfterConstructionCommand);
  inherited;
end;

function TCidadeServiceClient.save(Objeto: TCidade): TCidade;
begin
  if FsaveCommand = nil then
  begin
    FsaveCommand := FDBXConnection.CreateCommand;
    FsaveCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FsaveCommand.Text := 'TCidadeService.save';
    FsaveCommand.Prepare;
  end;
  if not Assigned(Objeto) then
    FsaveCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FsaveCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FsaveCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Objeto), True);
      if FInstanceOwner then
        Objeto.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FsaveCommand.ExecuteUpdate;
  if not FsaveCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FsaveCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCidade(FUnMarshal.UnMarshal(FsaveCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FsaveCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TCidadeServiceClient.update(Objeto: TCidade): TCidade;
begin
  if FupdateCommand = nil then
  begin
    FupdateCommand := FDBXConnection.CreateCommand;
    FupdateCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FupdateCommand.Text := 'TCidadeService.update';
    FupdateCommand.Prepare;
  end;
  if not Assigned(Objeto) then
    FupdateCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FupdateCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FupdateCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Objeto), True);
      if FInstanceOwner then
        Objeto.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FupdateCommand.ExecuteUpdate;
  if not FupdateCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FupdateCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCidade(FUnMarshal.UnMarshal(FupdateCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FupdateCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TCidadeServiceClient.delete(Objecto: TCidade): Boolean;
begin
  if FdeleteCommand = nil then
  begin
    FdeleteCommand := FDBXConnection.CreateCommand;
    FdeleteCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FdeleteCommand.Text := 'TCidadeService.delete';
    FdeleteCommand.Prepare;
  end;
  if not Assigned(Objecto) then
    FdeleteCommand.Parameters[0].Value.SetNull
  else
  begin
    FMarshal := TDBXClientCommand(FdeleteCommand.Parameters[0].ConnectionHandler).GetJSONMarshaler;
    try
      FdeleteCommand.Parameters[0].Value.SetJSONValue(FMarshal.Marshal(Objecto), True);
      if FInstanceOwner then
        Objecto.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FdeleteCommand.ExecuteUpdate;
  Result := FdeleteCommand.Parameters[1].Value.GetBoolean;
end;

function TCidadeServiceClient.findbyid(ID: Integer): TCidade;
begin
  if FfindbyidCommand = nil then
  begin
    FfindbyidCommand := FDBXConnection.CreateCommand;
    FfindbyidCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FfindbyidCommand.Text := 'TCidadeService.findbyid';
    FfindbyidCommand.Prepare;
  end;
  FfindbyidCommand.Parameters[0].Value.SetInt32(ID);
  FfindbyidCommand.ExecuteUpdate;
  if not FfindbyidCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDBXClientCommand(FfindbyidCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TCidade(FUnMarshal.UnMarshal(FfindbyidCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FfindbyidCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

procedure TCidadeServiceClient.AfterConstruction;
begin
  if FAfterConstructionCommand = nil then
  begin
    FAfterConstructionCommand := FDBXConnection.CreateCommand;
    FAfterConstructionCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FAfterConstructionCommand.Text := 'TCidadeService.AfterConstruction';
    FAfterConstructionCommand.Prepare;
  end;
  FAfterConstructionCommand.ExecuteUpdate;
end;


constructor TCidadeServiceClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TCidadeServiceClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TCidadeServiceClient.Destroy;
begin
  FreeAndNil(FsaveCommand);
  FreeAndNil(FupdateCommand);
  FreeAndNil(FdeleteCommand);
  FreeAndNil(FfindbyidCommand);
  FreeAndNil(FAfterConstructionCommand);
  inherited;
end;

end.


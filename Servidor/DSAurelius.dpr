program DSAurelius;
{$APPTYPE GUI}

{$R *.dres}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  uServidor in 'uServidor.pas' {Form1},
  uServerMethod in 'uServerMethod.pas' {ServerMethods1: TDSServerModule},
  uServerContainer in 'uServerContainer.pas' {ServerContainer1: TDataModule},
  uWebModule in 'uWebModule.pas' {WebModule1: TWebModule},
  uCliente in 'Models\uCliente.pas',
  uClienteDAO in 'Dao\uClienteDAO.pas',
  uGenericDAO in 'Dao\uGenericDAO.pas',
  uGenericService in 'Services\uGenericService.pas',
  uClienteService in 'Services\uClienteService.pas',
  uRegisterClasses in 'Register\uRegisterClasses.pas',
  uEndereco in 'Models\uEndereco.pas',
  uBasicEntity in 'Models\uBasicEntity.pas',
  uCidade in 'Models\uCidade.pas',
  uCidadeDAO in 'Dao\uCidadeDAO.pas',
  uCidadeService in 'Services\uCidadeService.pas';

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

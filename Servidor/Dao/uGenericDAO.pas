unit uGenericDAO;

interface

 uses
   Aurelius.Engine.ObjectManager;

  type
    TGenericDAO<T: class> = class
    private
      FObjManager: TObjectManager;
    public
       function save(Objeto: T):T;
       function update(Objeto: T):T;
       function delete(Objecto: T):Boolean;
       function findbyid(ID:variant):T;


       destructor Destroy; override;
       procedure AfterConstruction; override;
    end;

implementation

uses uServerContainer, System.SysUtils;

{ TGenericDAO<T> }

procedure TGenericDAO<T>.AfterConstruction;
begin
  inherited;
  FObjManager := Container.GetObjectManager;
end;


function TGenericDAO<T>.delete(Objecto: T): Boolean;
var
 Temp: T;
begin
  try
    try
      Temp := FObjManager.Merge<T>(Objecto) as T;
      FObjManager.Remove(Temp);
      Result := True;
    except
      Result := False;
    end;
  finally
    FreeAndNil(Temp);
  end;
end;

destructor TGenericDAO<T>.Destroy;
begin
  FreeAndNil(FObjManager);
  inherited;
end;

function TGenericDAO<T>.findbyid(ID: Variant): T;
begin
  Result := FObjManager.Find(T,ID) as T;
end;

function TGenericDAO<T>.save(Objeto: T): T;
begin
  try
    FObjManager.Save(Objeto);
    FObjManager.Flush;
    Result := Objeto as T;
  except
    Result := nil;
  end;
end;

function TGenericDAO<T>.update(Objeto: T): T;
begin
  try
    FObjManager.Update(Objeto);
    FObjManager.Flush;
    Result := Objeto as T;
  except
    Result := nil;
  end;
end;

end.

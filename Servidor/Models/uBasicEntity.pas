unit uBasicEntity;

interface

 uses
  Aurelius.Mapping.Attributes;

 type
   TEntidade = class
  private
    [Column('ID',[TColumnProp.Unique, TColumnProp.Required, TColumnProp.NoUpdate])]
    Fid: Integer;
    procedure Setid(const Value: Integer);
  published
     property id: Integer read Fid write Setid;
   end;

implementation

{ TEntidade }

procedure TEntidade.Setid(const Value: Integer);
begin
  Fid := Value;
end;

end.

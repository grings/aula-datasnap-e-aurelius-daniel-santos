unit uCidade;

interface

 uses
   Aurelius.Mapping.Attributes, uBasicEntity;

 Type
   [Entity]
   [Sequence('seq_cidade')]
   [Table('cidade')]
   [ID('FID',TIdGenerator.IdentityOrSequence)]
   TCidade = clasS(TEntidade)
  private
    [Column('codigo_ibge')]
    Fcodigo_ibge: integer;
    [Column('nome')]
    Fnome: string;
    procedure Setcodigo_ibge(const Value: integer);
    procedure Setnome(const Value: string);
   public
     property nome: string read Fnome write Setnome;
     property codigo_ibge: integer read Fcodigo_ibge write Setcodigo_ibge;
   end;

implementation

{ TCidade }

procedure TCidade.Setcodigo_ibge(const Value: integer);
begin
  Fcodigo_ibge := Value;
end;

procedure TCidade.Setnome(const Value: string);
begin
  Fnome := Value;
end;

end.

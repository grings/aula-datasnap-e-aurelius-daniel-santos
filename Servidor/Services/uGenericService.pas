unit uGenericService;

interface

  uses uGenericDAO, Classes;

  type
    {$METHODINFO ON}
    TGenericService<T:class> = class(TPersistent)
    private
      FDao: TGenericDAO<T>;
    public
       function save(Objeto: T):T;
       function update(Objeto: T):T;
       function delete(Objecto: T):Boolean;
       function findbyid(ID:integer):T;
    {$METHODINFO OFF}
       destructor Destroy; override;
       procedure AfterConstruction; override;
    end;

implementation

{ TGenericService<T> }

  uses System.SysUtils;

procedure TGenericService<T>.AfterConstruction;
begin
  inherited;
  FDao := TGenericDAO<T>.Create;
end;

function TGenericService<T>.delete(Objecto: T): Boolean;
begin
  Result := FDao.delete(Objecto);
end;

destructor TGenericService<T>.Destroy;
begin
  FreeAndNil(FDao);
  inherited;
end;

function TGenericService<T>.findbyid(ID: integer): T;
begin
  Result := FDao.findbyid(ID);
end;

function TGenericService<T>.save(Objeto: T): T;
begin
  Result := FDao.save(Objeto);
end;

function TGenericService<T>.update(Objeto: T): T;
begin
  Result := FDao.update(Objeto);
end;

end.

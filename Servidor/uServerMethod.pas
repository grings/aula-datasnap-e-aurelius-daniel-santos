unit uServerMethod;

interface

uses System.SysUtils, System.Classes, Datasnap.DSServer, Datasnap.DSAuth;

type
  TServerMethods1 = class(TDSServerModule)
  private
    { Private declarations }
  public
    { Public declarations }
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
  end;

implementation

{$R *.dfm}

uses System.StrUtils;

function TServerMethods1.EchoString(Value: string): string;
begin
  Result := Value;
end;

function TServerMethods1.ReverseString(Value: string): string;
var
 X: Integer;
begin
  try
    Result := System.StrUtils.ReverseString(Value);
    X := StrToInt('A');
  except
    on E:Exception do
      Begin
        E.Message := 'Nova Mensagem';
        raise;
      End;
  end;
end;

end.


unit TestuProxy;
{

  Delphi DUnit Test Case
  ----------------------
  This unit contains a skeleton test case class generated by the Test Case Wizard.
  Modify the generated code to correctly setup and call the methods from the unit 
  being tested.

}

interface

uses
  TestFramework, Data.DBXJSON, Data.DB, Data.DBXDBReaders, Data.SqlExpr,
  System.Classes, uProxy, uCliente, Data.DBXCDSReaders, System.SysUtils,
  Data.DBXJSONReflect, Datasnap.DSProxy, Data.DBXClient, Data.DBXCommon,
  Data.DBXDataSnap;

type
  // Test methods for class TClienteServiceClient

  TestTClienteServiceClient = class(TTestCase)
  strict private
    FClienteServiceClient: TClienteServiceClient;
    FCidadeServiceClient: TCidadeServiceClient;
  public
    FID: Integer;
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestCRUDCliente;
  end;

implementation

uses uDataModule, System.Generics.Collections, uEndereco, uCidade;

procedure TestTClienteServiceClient.SetUp;
begin
  DataModule1 := TDataModule1.Create(nil);

  Check(DataModule1.SQLConnection1.Connected,'Conexao nao ativada');

  FClienteServiceClient := TClienteServiceClient.Create(DataModule1.SQLConnection1.DBXConnection,False);
  FCidadeServiceClient  := TCidadeServiceClient.Create(DataModule1.SQLConnection1.DBXConnection,False);
end;

procedure TestTClienteServiceClient.TearDown;
begin
  FreeAndNil(DataModule1);

  FClienteServiceClient.Free;
  FClienteServiceClient := nil;
end;

procedure TestTClienteServiceClient.TestCRUDCliente;
var
  ReturnValue: TCliente;
  Cliente: TCliente;
  Endereco: TEndereco;
  Cidade, ReturnCidade: TCidade;
  ID, IDCidade:Integer;
  FDelete: Boolean;
begin
  // TODO: Setup method call parameters
  Cliente          := TCliente.Create;
  Cliente.nome     := 'Daniel';
  Cliente.telefone := '9114-2022';
  Cliente.email    := 'daniellemail.com';

  Cidade := TCidade.Create;
  Cidade.nome := 'Tubar�o';
  Cidade.codigo_ibge := 999;

  ReturnCidade := FCidadeServiceClient.save(Cidade);

  Endereco := TEndereco.Create;
  Endereco.rua := 'Ferreira Lima';
  Endereco.cep := '88701-100';
  Endereco.bairro := 'Oficinas';
  Endereco.cidade := ReturnCidade;

  Cliente.enderecos := TObjectList<TEndereco>.Create();
  Cliente.enderecos.Add(Endereco);

  ReturnValue := FClienteServiceClient.save(Cliente);
  Check(ReturnValue <> nil,'Objeto n�o Salvo com sucesso!');

  ID := ReturnValue.id;

  FreeAndNil(ReturnValue);

  ReturnValue := FClienteServiceClient.findbyid(ID);

  Check(ReturnValue <> nil,'Consulta n�o retornou o Objeto');

  ReturnValue.nome := 'Novo Nome';
  ReturnValue.enderecos.Items[0].rua := 'Ferreira Lima 2';

  FClienteServiceClient.update(ReturnValue);

  FreeAndNil(ReturnValue);

  ReturnValue := FClienteServiceClient.findbyid(ID);

  Check(ReturnValue.nome = 'Novo Nome','Altera��o n�o funcionou');
  Check(ReturnValue.enderecos.Items[0].rua = 'Ferreira Lima 2','Altera��o n�o funcionou no endereco!');


  FDelete := FClienteServiceClient.delete(ReturnValue);

  FCidadeServiceClient.delete(ReturnCidade);

  Check(FDelete = True,'Erro na remo��o do item');

  ReturnValue := FClienteServiceClient.findbyid(ID);

  Check(ReturnValue = nil,'Cliente n�o foi deletado corretamente!');
end;



initialization
  // Register any test cases with the test runner
  RegisterTest(TestTClienteServiceClient.Suite);
end.

